var fs = require('fs-extra', 3.0);
fs.readFile('hello.txt', 'utf8')
.then((data)=>{
    console.log('initial data is ',data)
})
.then(()=>{
    return fs.writeFile('hello.txt', 'something new to ponder upon', 'utf8');
})
.then(()=>{
    return fs.readFile('hello.txt', 'utf8')
})
.then((data)=>{
    console.log('new data is ',data)
})
.catch((err)=>{
    console.log(err);
})