var fs = require('fs');
fs.readFile('hello.txt', 'utf8', (err, data)=>{
    if(err) throw err;
    console.log('initial data', data);
    fs.writeFile('hello.txt', 'An apple a day keeps the doctor away', 'utf8', (err) =>{
        if(err) throw err;
        fs.readFile('hello.txt','utf8',(err,data)=>{
            if(err) throw err;
            console.log('new content is ',data);
        });
    });
});